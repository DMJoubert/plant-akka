package utilities.validation

import cats.data.ValidatedNel
import io.bitlevel.reliance.{Validator, _}
import models.{Breeder, Plant, Strain}
import monix.eval.Task

object UpdateValidator {
  implicit val validateBreederUpdate: Breeder.Update => Task[ValidatedNel[String, Breeder.Update]] = entity => {
    Validator[Breeder.Update]
      .ruleOpt(_.name)(nonEmpty)
      .run(entity)
  }

  implicit val validateStrainUpdate: Strain.Update => Task[ValidatedNel[String, Strain.Update]] = entity => {
    Validator[Strain.Update]
      .ruleOpt(_.name)(nonEmpty)
      .ruleOpt(_.breederId)(GenericValidators.isValidUuid)
      .run(entity)
  }

  implicit val validatePlantUpdate: Plant.Update => Task[ValidatedNel[String, Plant.Update]] = entity => {
    Validator[Plant.Update]
      .ruleOpt(_.strainId)(GenericValidators.isValidUuid)
      .ruleOpt(_.startDate)(GenericValidators.isValidDate)
      .run(entity)
  }
}

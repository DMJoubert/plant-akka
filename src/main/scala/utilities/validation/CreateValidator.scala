package utilities.validation

import cats.data.ValidatedNel
import io.bitlevel.reliance.{Validator, _}
import models.{Breeder, Plant, Strain}
import monix.eval.Task

object CreateValidator {
  implicit val validateBreederCreate: Breeder.Create => Task[ValidatedNel[String, Breeder.Create]] = entity => {
    Validator[Breeder.Create]
      .rule(_.name)(nonEmpty)
      .run(entity)
  }

  implicit val validateStrainCreate: Strain.Create => Task[ValidatedNel[String, Strain.Create]] = entity => {
    Validator[Strain.Create]
      .rule(_.name)(nonEmpty)
      .rule(_.breederId)(GenericValidators.isValidUuid)
      .run(entity)
  }

  implicit val validatePlantCreate: Plant.Create => Task[ValidatedNel[String, Plant.Create]] = entity => {
    Validator[Plant.Create]
      .rule(_.strainId)(GenericValidators.isValidUuid)
      .ruleOpt(_.startDate)(GenericValidators.isValidDate)
      .run(entity)
  }
}

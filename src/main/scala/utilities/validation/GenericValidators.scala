package utilities.validation

import io.bitlevel.reliance.rules.Rule

import scala.util.matching.Regex

object GenericValidators {
  private val validateUUID: Regex = """[\da-fA-F]{8}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{4}-[\da-fA-F]{12}""".r
  private val validateDate: Regex = """^(?:[1-9]\d{3}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1\d|2[0-8])|(?:0[13-9]|1[0-2])
                                     -(?:29|30)|(?:0[13578]|1[02])-31)|(?:[1-9]\d(?:0[48]|[2468][048]|[13579][26])|
                                     (?:[2468][048]|[13579][26])00)-02-29)T(?:[01]\d|2[0-3]):[0-5]\d:[0-5]\d(?:Z|[+-]
                                     [01]\d:[0-5]\d)$|""".r

  def isValidUuid: Rule[String] = new Rule[String] {
    override def reason(value: String, path: List[String]): String =
      s"${path.mkString(".")} with value $value is invalid"

    override def validate(value: String): Boolean =
      validateUUID.unapplySeq(value).isDefined
  }

  def isValidUuids: Rule[List[String]] = new Rule[List[String]] {
    override def reason(value: List[String], path: List[String]): String =
      s"${path.mkString(".")} with value $value is invalid"

    override def validate(value: List[String]): Boolean =
      value.map(validateUUID.unapplySeq).forall(_.isDefined)
  }

  def isValidDate: Rule[String] = new Rule[String] {
    override def reason(value: String, path: List[String]): String =
      s"${path.mkString(".")} with value $value is invalid"

    override def validate(value: String): Boolean = {
      validateDate.unapplySeq(value).isDefined
    }
  }
}

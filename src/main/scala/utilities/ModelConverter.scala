package utilities

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter
import java.util.UUID

import com.fullfacing.apollo.core.utilities.DateTimeHelpers
import models.{Breeder, Plant, Strain}

object ModelConverter {
  implicit val mapToBreeder: Breeder.Create => Breeder = b => {
    Breeder(
      id = UUID.randomUUID,
      name = b.name,
      createdAt = DateTimeHelpers.now,
      updatedAt = DateTimeHelpers.now
    )
  }

  implicit val mapToStrain: Strain.Create => Strain = s => {
    Strain(
      id = UUID.randomUUID,
      name = s.name,
      breederId = UUID.fromString(s.breederId),
      createdAt = DateTimeHelpers.now,
      updatedAt = DateTimeHelpers.now
    )
  }

  implicit val mapToPlant: Plant.Create => Plant = p => {
    Plant(
      id = UUID.randomUUID,
      startDate = {
        if (!p.startDate.isEmpty) OffsetDateTime.parse(p.startDate.get, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        else DateTimeHelpers.now
      },
      strainId = UUID.fromString(p.strainId),
      createdAt = DateTimeHelpers.now,
      updatedAt = DateTimeHelpers.now
    )
  }
}

package handles

import monix.execution.Scheduler

object Schedulers {
  implicit val scheduler: Scheduler = Scheduler.fixedPool("fix", 8)
  val io: Scheduler = Scheduler.io("io")
}

package handles

import com.fullfacing.apollo.core.health.HealthCheck
import com.fullfacing.apollo.mongodb.codecs.MongoCodecs
import com.fullfacing.apollo.mongodb.dao.environment.{MongoConfiguration, MongoConnection}
import com.mongodb.connection.ClusterConnectionMode
import databases.{BreederRepository, PlantRepository, StrainRepository}
import monix.eval.Task
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.Macros
import org.mongodb.scala.connection.ClusterSettings
import org.mongodb.scala.{MongoClientSettings, MongoDatabase, ReadPreference, ServerAddress, WriteConcern}

import scala.collection.JavaConverters._

object Mongo extends MongoConfiguration
  with PlantRepository
  with StrainRepository
  with BreederRepository
  with HealthCheck {

  // Server information
  private val MONGODB_HOST = "localhost"
  private val MONGODB_PORT = 27017
  private val MONGODB_DATABASE = "plants"

  private val addresses: List[ServerAddress] = List(ServerAddress(MONGODB_HOST, MONGODB_PORT))

  private val clusterSettings: ClusterSettings.Builder => ClusterSettings.Builder = { builder =>
    builder
      .hosts(addresses.asJava)
      .mode(ClusterConnectionMode.MULTIPLE)
  }

  private val breederCodecs = {
    import models.Breeder
    import models.Breeder._
    List(Macros.createCodecProviderIgnoreNone[Breeder],
      Macros.createCodecProviderIgnoreNone[Fetch],
      Macros.createCodecProviderIgnoreNone[Update])
  }

  private val strainCodecs = {
    import models.Strain
    import models.Strain._
    List(Macros.createCodecProviderIgnoreNone[Strain],
      Macros.createCodecProviderIgnoreNone[Fetch],
      Macros.createCodecProviderIgnoreNone[Update])
  }

  private val plantCodecs = {
    import models.Plant
    import models.Plant._
    List(Macros.createCodecProviderIgnoreNone[Plant],
      Macros.createCodecProviderIgnoreNone[Fetch],
      Macros.createCodecProviderIgnoreNone[Update])
  }

  private val codecs = MongoCodecs.from { _ =>
    fromRegistries(
      fromProviders(
        (breederCodecs ++ strainCodecs ++ plantCodecs).asJava
      )
    )
  }

  val clientSettings: MongoClientSettings.Builder = MongoClientSettings.builder
    .retryWrites(true)
    .codecRegistry(codecs)
    .writeConcern(WriteConcern.MAJORITY)
    .readPreference(ReadPreference.secondaryPreferred)

  final val connection: MongoConnection = new MongoConnection(clusterSettings, clientSettings)
  override protected val mDatabase: Task[MongoDatabase] = {
    connection.getDatabase(MONGODB_DATABASE).memoizeOnSuccess
  }

  override implicit val cBreeders: Mongo.BreederCollection = new BreederCollection()
  override implicit val cStrains: Mongo.StrainCollection = new StrainCollection()
  override implicit val cPlants: Mongo.PlantCollection = new PlantCollection()

  def connect(): Task[Unit] = {
    for {
      _ <- mDatabase
    } yield ()
  }

  override def isConnected(): Task[Boolean] = connection.isConnected
}

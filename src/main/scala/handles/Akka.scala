package handles

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import api.{BreederRoutes, PlantRoutes, StrainRoutes}
import handles.Logging.logger
import monix.eval.Task

object Akka {
  implicit val sys: ActorSystem = ActorSystem.create("ActorSystem")
  implicit val mat: ActorMaterializer = ActorMaterializer()

  private val AKKA_INTERFACE = "0.0.0.0"
  private val AKKA_PORT = 9000
  private val AKKA_ROUTES = BreederRoutes.routes ~ StrainRoutes.routes ~ PlantRoutes.routes

  def connect(): Task[Unit] = Task.deferFutureAction { implicit ctx =>
    Http().bindAndHandle(AKKA_ROUTES, AKKA_INTERFACE, AKKA_PORT).map { binding =>
      logger.info(s"Host API listening on ${binding.localAddress.getHostString}:${binding.localAddress.getPort}")
    }
  }
}
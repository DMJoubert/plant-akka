package models

import java.time.OffsetDateTime
import java.util.UUID

import com.fullfacing.apollo.core.entities.DomainModel

/**
  * @param breederId     UUID representing what Breeder creates this Strain
  * @param name          Name of the Strain
  */
case class Strain(id: UUID,
                  breederId: UUID,
                  name: String,
                  deleted: Boolean = false,
                  createdAt: OffsetDateTime,
                  updatedAt: OffsetDateTime) extends DomainModel

object Strain {
  final case class Create(breederId: String,
                          name: String)

  final case class Update(breederId: Option[String] = None,
                          name: Option[String] = None)

  final case class Fetch(id: Option[UUID] = None,
                         breederId: Option[UUID] = None,
                         name: Option[String] = None,
                         createdAt: Option[OffsetDateTime] = None,
                         updatedAt: Option[OffsetDateTime] = None)
}

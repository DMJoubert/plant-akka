package models

import java.time.OffsetDateTime
import java.util.UUID

import com.fullfacing.apollo.core.entities.DomainModel

/**
  * @param strainId      UUID representing the Strain of this Plant
  * @param startDate     When the Plant was first planted
  */
case class Plant(id: UUID,
                 strainId: UUID,
                 startDate: OffsetDateTime,
                 deleted: Boolean = false,
                 createdAt: OffsetDateTime,
                 updatedAt: OffsetDateTime) extends DomainModel

object Plant {
  final case class Create(strainId: String,
                          startDate: Option[String] = None)

  final case class Update(strainId: Option[String] = None,
                          startDate: Option[String] = None)

  final case class Fetch(id: Option[UUID] = None,
                         strainId: Option[UUID] = None,
                         startDate: Option[OffsetDateTime] = None,
                         createdAt: Option[OffsetDateTime] = None,
                         updatedAt: Option[OffsetDateTime] = None)
}
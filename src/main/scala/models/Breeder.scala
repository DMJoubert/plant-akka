package models

import java.time.OffsetDateTime
import java.util.UUID

import com.fullfacing.apollo.core.entities.DomainModel

/**
  * @param name     Name of the Breeder as a String
  */
final case class Breeder(id: UUID,
                         name: String,
                         deleted: Boolean = false,
                         createdAt: OffsetDateTime,
                         updatedAt: OffsetDateTime) extends DomainModel

object Breeder {
  final case class Create(name: String)

  final case class Update(name: Option[String] = None)

  final case class Fetch(id: Option[UUID] = None,
                         name: Option[String] = None,
                         createdAt: Option[OffsetDateTime] = None,
                         updatedAt: Option[OffsetDateTime] = None)
}

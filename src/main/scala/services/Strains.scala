package services

import com.fullfacing.apollo.core.json.JsonFormats.default
import com.fullfacing.apollo.http.rest.requests.Operations
import handles.Logging.logger
import handles.Mongo
import models.Strain

object Strains extends Operations[Strain](Mongo.cStrains)

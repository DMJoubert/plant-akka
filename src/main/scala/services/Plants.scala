package services

import com.fullfacing.apollo.http.rest.requests.Operations
import com.fullfacing.apollo.core.json.JsonFormats.default
import handles.Logging.logger
import handles.Mongo
import models.Plant

object Plants extends Operations[Plant](Mongo.cPlants)

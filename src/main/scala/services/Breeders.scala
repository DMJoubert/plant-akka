package services

import com.fullfacing.apollo.core.json.JsonFormats.default
import com.fullfacing.apollo.http.rest.requests.Operations
import handles.Logging.logger
import handles.Mongo
import models.Breeder

object Breeders extends Operations[Breeder](Mongo.cBreeders)

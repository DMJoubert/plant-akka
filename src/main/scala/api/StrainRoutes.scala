package api

import akka.http.scaladsl.server.Directives.{entity, _}
import com.fullfacing.apollo.core.json.JsonFormats.default
import com.fullfacing.apollo.http.directives.ConcurrencyControl
import com.fullfacing.apollo.http.directives.Directives.{query, _}
import com.fullfacing.apollo.http.marshallers.Json4sSupport
import com.fullfacing.apollo.http.marshallers.SequenceMarshallers.CsvList
import com.fullfacing.apollo.http.rest.BaseUri._
import com.fullfacing.apollo.http.rest.HttpService
import com.fullfacing.apollo.http.rest.HttpService.RequestHandler
import com.fullfacing.apollo.mongodb.query.DefaultParameterFormats
import handles.Mongo
import handles.Schedulers.scheduler
import models.Strain
import services.Strains
import utilities.ModelConverter
import utilities.validation.{CreateValidator, UpdateValidator}

object StrainRoutes extends HttpService("v1" - "strains") with Json4sSupport {

  implicit val defaultParameterFormats = DefaultParameterFormats

  override val api: RequestHandler = { implicit context =>
    get {
      path(JavaUUID) { id =>
        parameter('fields.as(CsvList[String]) ? List.empty[String]) { fields =>
          validateCache(id, ConcurrencyControl.cache(Mongo.cStrains)) {
            onCompleteWithResponse(Strains.fetchById[Strain.Fetch](id, fields))
          }
        }
      } ~
        pathEndOrSingleSlash {
          query(()) { q =>
            onCompleteWithResponse(Strains.fetch[Strain.Fetch](q))
          }
        }
    } ~
    post {
      pathEndOrSingleSlash {
        entity(as[Strain.Create]) { body =>
          onCompleteWithResponse(Strains.create[Strain.Create](body)
            (CreateValidator.validateStrainCreate, ModelConverter.mapToStrain))
        }
      }
    } ~
    patch {
      path(JavaUUID) { id =>
        validateUpdate(id, ConcurrencyControl.cache(Mongo.cStrains)) {
          asJsonMergePatch[Strain.Update](()) { request =>
            onCompleteWithResponse(Strains.update[Strain.Update](id, request)(UpdateValidator.validateStrainUpdate))
          }
        }
      }
    } ~
    delete {
      path(JavaUUID) { id =>
        onCompleteWithResponse(Strains.delete(id))
      }
    }
  }
}

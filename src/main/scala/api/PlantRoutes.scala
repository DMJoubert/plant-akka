package api

import akka.http.scaladsl.server.Directives.{entity, _}
import com.fullfacing.apollo.core.json.JsonFormats.default
import com.fullfacing.apollo.http.directives.ConcurrencyControl
import com.fullfacing.apollo.http.directives.Directives.{query, _}
import com.fullfacing.apollo.http.marshallers.Json4sSupport
import com.fullfacing.apollo.http.marshallers.SequenceMarshallers.CsvList
import handles.Schedulers.scheduler
import com.fullfacing.apollo.http.rest.BaseUri._
import com.fullfacing.apollo.http.rest.HttpService
import com.fullfacing.apollo.http.rest.HttpService.RequestHandler
import com.fullfacing.apollo.mongodb.query.DefaultParameterFormats
import handles.Mongo
import models.Plant
import services.Plants
import utilities.ModelConverter
import utilities.validation.{CreateValidator, UpdateValidator}

object PlantRoutes extends HttpService("v1" - "plants") with Json4sSupport {
  implicit val defaultParameterFormats = DefaultParameterFormats

  override val api: RequestHandler = { implicit context =>
    get {
      path(JavaUUID) { id =>
        parameter('fields.as(CsvList[String]) ? List.empty[String]) { fields =>
          validateCache(id, ConcurrencyControl.cache(Mongo.cPlants)) {
            onCompleteWithResponse(Plants.fetchById[Plant.Fetch](id, fields))
          }
        }
      } ~
      pathEndOrSingleSlash {
        query(()) { q =>
          onCompleteWithResponse(Plants.fetch[Plant.Fetch](q))
        }
      }
    } ~
    post {
      pathEndOrSingleSlash {
        entity(as[Plant.Create]) { body =>
          onCompleteWithResponse(Plants.create[Plant.Create](body)
            (CreateValidator.validatePlantCreate, ModelConverter.mapToPlant))
        }
      }
    } ~
    patch {
      path(JavaUUID) { id =>
        validateUpdate(id, ConcurrencyControl.cache(Mongo.cPlants)) {
          asJsonMergePatch[Plant.Update](()) { request =>
            onCompleteWithResponse(Plants.update[Plant.Update](id, request)(UpdateValidator.validatePlantUpdate))
          }
        }
      }
    } ~
    delete {
      path(JavaUUID) { id =>
        onCompleteWithResponse(Plants.delete(id))
      }
    }
  }
}

package api

import akka.http.scaladsl.server.Directives.{entity, _}
import com.fullfacing.apollo.core.json.JsonFormats.default
import com.fullfacing.apollo.http.directives.ConcurrencyControl
import com.fullfacing.apollo.http.directives.Directives.{query, _}
import com.fullfacing.apollo.http.marshallers.Json4sSupport
import com.fullfacing.apollo.http.marshallers.SequenceMarshallers.CsvList
import com.fullfacing.apollo.http.rest.BaseUri._
import com.fullfacing.apollo.http.rest.HttpService
import com.fullfacing.apollo.http.rest.HttpService.RequestHandler
import com.fullfacing.apollo.mongodb.query.DefaultParameterFormats
import handles.Mongo
import handles.Schedulers.scheduler
import models.Breeder
import services.Breeders
import utilities.ModelConverter
import utilities.validation.{CreateValidator, UpdateValidator}


object BreederRoutes extends HttpService("v1" - "breeders") with Json4sSupport {

  implicit val defaultParameterFormats = DefaultParameterFormats

  override val api: RequestHandler = { implicit context =>
    get {
      path(JavaUUID) { id =>
        parameter('fields.as(CsvList[String]) ? List.empty[String]) { fields =>
          validateCache(id, ConcurrencyControl.cache(Mongo.cBreeders)) {
            onCompleteWithResponse(Breeders.fetchById[Breeder.Fetch](id, fields))
          }
        }
      } ~
      pathEndOrSingleSlash {
        query(()) { q =>
          onCompleteWithResponse(Breeders.fetch[Breeder.Fetch](q))
        }
      }
    } ~
    post {
      pathEndOrSingleSlash {
        entity(as[Breeder.Create]) { body =>
          onCompleteWithResponse(Breeders.create[Breeder.Create](body)
            (CreateValidator.validateBreederCreate, ModelConverter.mapToBreeder))
        }
      }
    } ~
    patch {
      path(JavaUUID) { id =>
        validateUpdate(id, ConcurrencyControl.cache(Mongo.cBreeders)) {
          asJsonMergePatch[Breeder.Update](()) { request =>
            onCompleteWithResponse(Breeders.update[Breeder.Update](id, request)(UpdateValidator.validateBreederUpdate))
          }
        }
      }
    } ~
    delete {
      path(JavaUUID) { id =>
        onCompleteWithResponse(Breeders.delete(id))
      }
    }
  }
}

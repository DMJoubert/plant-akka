package databases

import com.fullfacing.apollo.mongodb.dao.MongoCrudService
import com.fullfacing.apollo.mongodb.dao.environment.MongoConfiguration
import databases.repository.RepositoryLayer
import models.Plant
import monix.eval.Task
import org.mongodb.scala.MongoCollection

trait PlantRepository { self: MongoConfiguration =>
  val cPlants: PlantCollection

  class PlantCollection extends MongoCrudService[Plant] with RepositoryLayer[Plant] {
    override val collection: Task[MongoCollection[Plant]] = mDatabase.map { database =>
      database.getCollection[Plant]("plants")
    }.memoizeOnSuccess
  }

}
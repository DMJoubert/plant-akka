package databases.repository

import com.fullfacing.apollo.core.entities.DomainModel
import com.fullfacing.apollo.mongodb.dao.MongoCrudService

trait RepositoryLayer[A <: DomainModel] { self: MongoCrudService[A] => }

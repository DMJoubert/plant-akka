package databases

import com.fullfacing.apollo.mongodb.dao.MongoCrudService
import com.fullfacing.apollo.mongodb.dao.environment.MongoConfiguration
import databases.repository.RepositoryLayer
import models.Breeder
import monix.eval.Task
import org.mongodb.scala.MongoCollection

trait BreederRepository { self: MongoConfiguration =>
  val cBreeders: BreederCollection

  class BreederCollection extends MongoCrudService[Breeder] with RepositoryLayer[Breeder] {
    override val collection: Task[MongoCollection[Breeder]] = mDatabase.map { database =>
      database.getCollection[Breeder]("breeders")
    }.memoizeOnSuccess
  }

}

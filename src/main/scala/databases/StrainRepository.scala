package databases

import com.fullfacing.apollo.mongodb.dao.MongoCrudService
import com.fullfacing.apollo.mongodb.dao.environment.MongoConfiguration
import databases.repository.RepositoryLayer
import models.Strain
import monix.eval.Task
import org.mongodb.scala.MongoCollection

trait StrainRepository { self: MongoConfiguration =>
  val cStrains: StrainCollection

  class StrainCollection extends MongoCrudService[Strain] with RepositoryLayer[Strain] {
    override val collection: Task[MongoCollection[Strain]] = mDatabase.map { database =>
      database.getCollection[Strain]("strains")
    }.memoizeOnSuccess
  }

}

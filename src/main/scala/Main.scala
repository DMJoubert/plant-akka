import cats.effect.ExitCode
import handles._
import monix.eval.{Task, TaskApp}

object Main extends TaskApp {
  override def run(args: List[String]): Task[ExitCode] = {
    for {
      _ <- Mongo.connect
      _ <- Akka.connect
    } yield ExitCode.Success
  }
}

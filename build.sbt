name := "plant-akka"

version := "0.1"

scalaVersion := "2.12.8"

val scalacOpts = Seq(
  "-Ywarn-unused:implicits",
  "-Ywarn-unused:imports",
  "-Ywarn-unused:locals",
  "-Ywarn-unused:params",
  "-Ywarn-unused:patvars",
  "-Ywarn-unused:privates",
  "-Ypartial-unification",
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture"
)

scalacOptions ++= scalacOpts

addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.4")
addCompilerPlugin("com.olegpy" %% "better-monadic-for" % "0.2.4")

resolvers ++= {
  val nexusURL = sys.env.getOrElse("NEXUS_REPO_URL", "nexus.k8s.dev.fin.fullfacing.com")
  Seq(
    "Sonatype OSS Releases" at s"https://$nexusURL/repository/maven-releases",
    "Sonatype OSS Snapshots" at s"https://$nexusURL/repository/maven-snapshots",
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
}

val apollo = {
  val version = "2.14.0-SNAPSHOT"
  Seq(
    "com.fullfacing" %% "apollo-core"     % version,
    "com.fullfacing" %% "apollo-http"     % version,
    "com.fullfacing" %% "apollo-redis"    % version,
    "com.fullfacing" %% "apollo-mongodb"  % version,
    "com.fullfacing" %% "apollo-security" % version,
    "com.fullfacing" %% "apollo-rabbitmq" % version
  )
}

libraryDependencies ++= {
  apollo
}